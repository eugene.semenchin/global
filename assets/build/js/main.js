// Input ограничение символов // 

const input = document.querySelector('.search__input');
input.addEventListener('input', () => {
    let inputValue = input.value.replace(/[!,@,#,$,%,^,&,*,(,)]/g, '');
    input.value = inputValue;
})

// Ajax запрос // 

function getData() {
    const requestURL = 'https://baconipsum.com/api/?type=lucky';
    const xhr = new XMLHttpRequest();
    xhr.open('GET', requestURL);
    xhr.onload = () => {
      if (xhr.status !== 200) {
        return;
      }
        document.querySelector('.main__text').innerHTML = xhr.response;
    } 
    
    xhr.send();
}
getData();

// Burger меню // 

const openBtn = document.querySelector('.nav__btn');
const closeBtn = document.querySelector('.close__btn');
const navMenu = document.querySelector('.nav');

openBtn.addEventListener('click', () => {
  navMenu.classList.add('active');
})

closeBtn.addEventListener('click', () => {
  navMenu.classList.remove('active');
})